import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

word_dim = 200

avg_rating = 4.2297
num_users = 19853
num_items = 8510

vocab_sz = 16602
seq_len = 11
sent_num = 8
user_seq_num = 9
item_seq_num = 22

kernel_size = 3
filters_num = 100
embedding_dim = 32
dropout = 0.5
weight_decay = 0.02
learning_rate = 0.002
batch_size = 256
train_epochs = 30

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path