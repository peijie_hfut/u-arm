import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import random

import config_uarm as conf 

PAD = 0; SOS = 1; EOS = 2

def tensorToScalar(tensor):
    return tensor.cpu().detach().numpy()

margin_ranking_loss = nn.MarginRankingLoss(margin=1.0, reduction='mean')
mse_loss = nn.MSELoss(reduction='sum')

class encoder_2(nn.Module):
    def __init__(self):
        super(encoder_2, self).__init__()

        self.word_embedding = nn.Embedding(conf.abae_vocab_sz, conf.abae_word_dim)
        self.word_embedding.weight.requires_grad = False
        
        self.transform_M = nn.Linear(conf.abae_word_dim, conf.abae_word_dim, bias=False) # weight: word_dimension * word_dimension
        self.transform_W = nn.Linear(conf.abae_word_dim, conf.asp_dim) # weight: aspect_dimension * word_diension
        self.transform_T = nn.Linear(conf.asp_dim, conf.abae_word_dim, bias=False) # weight: word_dimension * aspect_dimension

        self.reinit()

    def reinit(self):
        nn.init.zeros_(self.transform_W.bias)

        nn.init.xavier_uniform_(self.transform_M.weight)
        nn.init.xavier_uniform_(self.transform_W.weight)
        #nn.init.xavier_uniform_(self.transform_T.weight)

    # e_w: (batch_size, sequence_length, word_dimension)
    # Y_s: (batch_size, word_dimension)
    # pos_rev: (batch_size, sequence_length)
    def _attention(self, pos_rev, e_w, y_s):
        mask = (pos_rev > 0).long() # (batch_size, seq_len)

        # self.transform_M(e_w): (batch_size, sequence_length, word_dimension)
        dx = torch.matmul(self.transform_M(e_w), y_s).view(-1, conf.seq_len) # (batch_size, sequence_length)
        dx_mask = (dx < 80).long()
        dx = dx_mask * dx

        ax_1 = torch.exp(dx) # (batch_size, seq_len)
        ax_2 = ax_1 * mask # (batch_size, seq_len)
        ax_3 = torch.sum(ax_2, dim=1, keepdim=True) + 1e-6 # (batch_size, 1)
        ax_4 = (ax_2 / ax_3).view(-1, conf.seq_len, 1) # (batch_size, seq_len, 1)
        
        # e_w.transpose(1, 2): (batch_size, word_dimension, sequence_length)
        # torch.matmul(e_w, a): (batch_size, word_dimension, 1)
        z_s = torch.sum(e_w*ax_4, dim=1).view(-1, conf.abae_word_dim) # (batch_size, word_dimension)

        return z_s

    # w: (batch_size, sequence_length)
    # y_s: (batch_size, word_dimension)
    # z_n: (batch_size * num_negative_reviews, word_dimension)
    def forward(self, pos_rev, neg_rev, epoch):
        #positive_review: (batch_size, sequence_length)
        #negative_review: (batch_size*num_negative_reviews, sequence_length)

        pos_rev_emb = self.word_embedding(pos_rev) # (batch_size, sequence_length, word_dimension)
        neg_rev_emb = self.word_embedding(neg_rev) # (batch_size*num_negative_reviews, sequence_length, word_dimension)

        y_s = torch.sum(pos_rev_emb, 1) # (batch_size, word_dimension, 1)
        z_n = torch.sum(neg_rev_emb, 1) # (batch_size * num_negative_reviews, word_dimension)
        
        pos_rev_mask = (pos_rev > 0).long()
        neg_rev_mask = (neg_rev > 0).long()

        pos_rev_mask = torch.sum(pos_rev_mask, dim=1, keepdim=True) + 1e-6
        neg_rev_mask = torch.sum(neg_rev_mask, dim=1, keepdim=True) + 1e-6

        y_s = (y_s / pos_rev_mask).view(-1, conf.abae_word_dim, 1)
        z_n = (z_n / neg_rev_mask).view(-1, conf.abae_word_dim)

        z_s = self._attention(pos_rev, pos_rev_emb, y_s) # (batch_size, word_dimension)
        
        #p_t = self.transform_W(z_s)
        p_t = F.softmax(self.transform_W(z_s), dim=1) # (batch_size, aspect_dimension)
        r_s = self.transform_T(p_t) # (batch_size, word_dimension)

        # cosine similarity betwee r_s and z_s
        c1 = (F.normalize(r_s, p=2, dim=1) * F.normalize(z_s, p=2, dim=1)).sum(-1, keepdim=True) # (batch_size, 1)
        c1 = c1.repeat(1, conf.num_neg_sent).view(-1) # (batch_size * num_negative)

        # z_n.view(conf.batch_size, conf.num_negative_reviews, -1): (batch_size, num_negative_reviews, word_dimension)
        # r_s.view(conf.batch_size, 1, -1): (batch_size, 1, word_dimension)
        # z_n * r_s: (batch_size, num_negative_reviews, word_dimension)
        # (z_n * r_s).sum(-1): (batch_size, num_negative)
        # (z_n * r_s).sum(-1).view(-1): (batch_size)
        c2 = (F.normalize(z_n.view(y_s.shape[0], conf.num_neg_sent, -1), p=2, dim=2) \
             * F.normalize(r_s.view(y_s.shape[0], 1, -1), p=2, dim=2)).sum(-1).view(-1) # (batch_size * num_negative)
        
        J_loss = margin_ranking_loss(c1, c2, torch.FloatTensor([1.0]).cuda())
    
        transform_T_weight = F.normalize(self.transform_T.weight, p=2, dim=0) # word_dimension * asp_dim
        U_loss = mse_loss(torch.matmul(torch.transpose(transform_T_weight, 0, 1), transform_T_weight), torch.eye(conf.asp_dim).cuda())

        return p_t, J_loss, U_loss

class encoder_1(nn.Module):
    def __init__(self):
        super(encoder_1, self).__init__()

        self.gamma_user_embedding = nn.Embedding(conf.num_users, conf.att_dim)
        self.gamma_item_embedding = nn.Embedding(conf.num_items, conf.att_dim)

        self.beta_user_embedding = nn.Embedding(conf.num_users, conf.aspect_dim)
        self.beta_item_embedding = nn.Embedding(conf.num_items, conf.aspect_dim)

        self.linear_eq_3 = nn.Linear(2*conf.att_dim, conf.hidden_dim)
        self.linear_eq_4 = nn.Linear(2*conf.aspect_dim, conf.hidden_dim)

        #self.rnn = nn.GRU(conf.word_dim, conf.hidden_dim, num_layers=1, dropout=conf.dropout)
        self.rnn = nn.GRU(conf.word_dim, conf.hidden_dim, num_layers=1)

        self.dropout = nn.Dropout(conf.dropout)
        self.reinit()

    def reinit(self):
        nn.init.xavier_normal_(self.gamma_user_embedding.weight)
        nn.init.xavier_normal_(self.gamma_item_embedding.weight)
        nn.init.xavier_normal_(self.beta_user_embedding.weight)
        nn.init.xavier_normal_(self.beta_item_embedding.weight)

        nn.init.xavier_normal_(self.linear_eq_3.weight)
        nn.init.xavier_normal_(self.linear_eq_4.weight)
        nn.init.zeros_(self.linear_eq_3.bias)
        nn.init.zeros_(self.linear_eq_4.bias)

        # initialize the parameters in self.rnn
        for name, param in self.rnn.named_parameters():
            if 'weight' in name:
                nn.init.xavier_normal_(param.data)
            else:
                nn.init.zeros_(param.data)

    def forward(self, user, item):
        gamma_u = self.gamma_user_embedding(user) # (batch_size, att_dim)
        gamma_i = self.gamma_item_embedding(item) # (batch_size, att_dim)

        beta_u = self.beta_user_embedding(user) # (batch_size, aspect_dim)
        beta_i = self.beta_item_embedding(item) # (batch_size, aspect_dim)

        u_vector = torch.tanh(self.linear_eq_3(torch.cat([gamma_u, gamma_i], 1))) # (batch_size, hidden_dim)
        v_vector = torch.tanh(self.linear_eq_4(torch.cat([beta_u, beta_i], 1))) # (batch_size, hidden_dim)
        
        hidden_state = (u_vector + v_vector).view(1, user.shape[0], conf.hidden_dim) # (1, batch, hidden_size=n)
        #hidden_state = (u_vector).view(1, user.shape[0], conf.hidden_dim) # (1, batch, hidden_size=n)

        '''1_REVIEW GENERATION ATTENTION PLEASE!!!'''
        #### START ------ ****** verify review generation with GRU ****** ####
        #### FIRST PART #### 
        '''
        hidden_state = torch.zeros(1, user.shape[0], conf.hidden_dim).cuda() ### 
        '''
        #### ****** verify review generation with GRU ****** ------ END ####

        return gamma_u, gamma_i, beta_u, beta_i, hidden_state


class decoder_1(nn.Module):
    def __init__(self):
        super(decoder_1, self).__init__()
        #self.rnn = nn.GRU(conf.word_dim, conf.hidden_dim, num_layers=1, dropout=conf.dropout)
        self.rnn = nn.GRU(conf.word_dim, conf.hidden_dim, num_layers=1)

        self.linear_eq_10 = nn.Linear(conf.att_dim+conf.hidden_dim, 1)
        self.linear_eq_11 = nn.Linear(2*conf.aspect_dim, conf.aspect_dim)
        self.linear_eq_12 = nn.Linear(1*conf.asp_dim+1*conf.aspect_dim+0*conf.word_dim+1*conf.hidden_dim, conf.aspect_dim)
        self.linear_eq_13 = nn.Linear(conf.hidden_dim+1*conf.att_dim, conf.vocab_sz)

        self.linear_eq_asp = nn.Linear(2*conf.asp_dim, conf.asp_dim)

        self.linear_x = nn.Linear(conf.hidden_dim, conf.vocab_sz)

        self.dropout = nn.Dropout(conf.dropout)

        '''2_REVIEW GENERATION ATTENTION PLEASE!!!'''
        #### START ------ ****** verify review generation with GRU ****** ####
        #### SECOND PART #### 
        '''
        self.rnn_out_linear = nn.Linear(conf.hidden_dim, conf.vocab_sz) ### 
        '''
        #### ****** verify review generation with GRU ****** ------ END ####

        self.reinit()

    def reinit(self):
        for name, param in self.rnn.named_parameters():
            if 'weight' in name:
                nn.init.xavier_uniform_(param.data)
            else:
                nn.init.zeros_(param.data)
        
        nn.init.zeros_(self.linear_eq_11.bias)
        nn.init.xavier_uniform_(self.linear_eq_11.weight)
        
        nn.init.zeros_(self.linear_eq_asp.bias)
        nn.init.xavier_uniform_(self.linear_eq_asp.weight)

        nn.init.zeros_(self.linear_eq_12.bias)
        nn.init.xavier_uniform_(self.linear_eq_12.weight)

        nn.init.zeros_(self.linear_eq_10.bias)
        nn.init.xavier_uniform_(self.linear_eq_10.weight)

        nn.init.zeros_(self.linear_eq_13.bias)
        nn.init.xavier_uniform_(self.linear_eq_13.weight)

        nn.init.zeros_(self.linear_x.bias)
        nn.init.xavier_uniform_(self.linear_x.weight)

        '''3_REVIEW GENERATION ATTENTION PLEASE!!!'''
        #### START ------ ****** verify review generation with GRU ****** ####
        #### THIRD PART #### 
        '''
        nn.init.xavier_uniform_(self.rnn_out_linear.weight)
        nn.init.zeros_(self.rnn_out_linear.bias) ### 
        '''
        #### ****** verify review generation with GRU ****** ------ END ####

    # value: (seq_len, batch, hidden_dim)
    # query: (1, batch, hidden_dim)
    def _attention(self, value, query, mask, func):
        query = query.repeat(value.shape[0], 1, 1) # (seq_len, batch, hidden_dim) 
        key = torch.cat([value, query], dim=2)  # (seq_len, batch, hidden_dim*2)
        
        a_1 = torch.tanh(func(key)).view(value.shape[0], -1)  # (seq_len, batch)
        a_2 = torch.exp(a_1) #  (seq_len, batch)
        a_3 = a_2 * mask # (seq_len, batch)
        a_4 = torch.sum(a_3, dim=0, keepdim=True) + 1e-6 # (1, batch)
        a_5 = (a_3 / a_4).view(value.shape[0], -1, 1) # (seq_len, batch, 1)
        a_t = torch.sum(a_5 * value, dim=0) # (batch, hidden_dim)

        return a_t

    def forward(self, 
            input_vector, 
            hidden_state, 
            gamma_u,
            gamma_i,
            beta_u, 
            beta_i, 
            review_aspect_mask, aspect_user_embed, aspect_item_embed
        ):
        input_vector = self.dropout(input_vector)
        output, hidden_state = self.rnn(input_vector, hidden_state)
        # calculate a^2_t
        mask = torch.ones(2, gamma_u.shape[0]).cuda()
        a_2_t = self._attention(torch.stack([gamma_u, gamma_i], dim=0), hidden_state, mask, self.linear_eq_10) #(batch, att_dim)
        # calculate a^3_t
        s_ui = self.linear_eq_11(torch.cat([beta_u, beta_i], dim=1)) # (batch, aspect_dim)
        #a_3_t = torch.tanh(self.linear_eq_12(torch.cat([s_ui, \
        #    input_vector.view(-1, conf.word_dim), hidden_state.view(-1, conf.hidden_dim)], dim=1))) # (batch, aspect_dim)
        
        asp_ui = self.linear_eq_asp(torch.cat([aspect_user_embed, aspect_item_embed], dim=1))

        #a_3_t = (self.linear_eq_12(torch.cat([s_ui, hidden_state.view(-1, conf.hidden_dim),\
        #    input_vector.view(-1, conf.word_dim)], dim=1))) # (batch, aspect_dim)

        a_3_t = (self.linear_eq_12(torch.cat([asp_ui, s_ui, hidden_state.view(-1, conf.hidden_dim)], dim=1))) # (batch, aspect_dim)
        
        #PvWt = torch.tanh(self.linear_eq_13(torch.cat([hidden_state.view(-1, conf.hidden_dim), a_1_t, a_2_t], dim=1))) # (batch, vocab_size)
        PvWt = self.linear_eq_13(torch.cat([hidden_state.view(-1, conf.hidden_dim), a_2_t], dim=1)) # (batch, vocab_size)

        #PvWt = self.linear_eq_13(torch.cat([hidden_state.view(-1, conf.hidden_dim)], dim=1)) # (batch, vocab_size)

        #import pdb; pdb.set_trace()
        #aspect_probit = torch.index_select(a_3_t, 1, review_aspect) * review_aspect_mask # (seq_length*batch_size, vocab_sz)
        
        x_aspect_probit = torch.sparse.mm(review_aspect_mask, a_3_t.t()).t() #batch, vocab_sz
        #import  pdb; pdb.set_trace()

        word_probit = self.linear_x(hidden_state.view(-1, conf.hidden_dim)) # (batch, vocab_sz)

        '''4_REVIEW GENERATION ATTENTION PLEASE!!!'''
        #### START ------ ****** verify review generation with GRU ****** ####
        #### FOURTH PART #### 
        '''
        word_probit = self.rnn_out_linear(hidden_state.view(-1, conf.hidden_dim)) ### 
        '''
        #### ****** verify review generation with GRU ****** ------ END ####

        return PvWt + 1.0*x_aspect_probit, hidden_state
        #return word_probit, hidden_state
        #return PvWt, hidden_state

class decoder_fm(nn.Module):
    def __init__(self):
        super(decoder_fm, self).__init__()
        
        self.user_fc_linear = nn.Linear(conf.asp_dim, conf.mf_dim)
        self.item_fc_linear = nn.Linear(conf.asp_dim, conf.mf_dim)
        
        self.gmf_user_embedding = nn.Embedding(conf.num_users, conf.gmf_embed_dim)
        self.gmf_item_embedding = nn.Embedding(conf.num_items, conf.gmf_embed_dim)

        #self.linears = []
        #for idx in range(1, len(conf.mlp_dim_list)):
        #    self.linears.append(nn.Linear(conf.mlp_dim_list[idx-1], conf.mlp_dim_list[idx], bias=False).cuda())
        self.linear_1 = nn.Linear(conf.mlp_dim_list[0], conf.mlp_dim_list[1], bias=False)
        self.linear_2 = nn.Linear(conf.mlp_dim_list[1], conf.mlp_dim_list[2], bias=False)
        self.linear_3 = nn.Linear(conf.mlp_dim_list[2], conf.mlp_dim_list[3], bias=False)

        #self.x_linears = []
        #for idx in range(1, len(conf.mlp_dim_list)):
        #    self.x_linears.append(nn.Linear(conf.mlp_dim_list[idx-1], conf.mlp_dim_list[idx], bias=False).cuda())
        self.x_linear_1 = nn.Linear(conf.mlp_dim_list[0], conf.mlp_dim_list[1], bias=False)
        self.x_linear_2 = nn.Linear(conf.mlp_dim_list[1], conf.mlp_dim_list[2], bias=False)
        self.x_linear_3 = nn.Linear(conf.mlp_dim_list[2], conf.mlp_dim_list[3], bias=False)

        self.x_final_linear = nn.Linear(1*conf.gmf_embed_dim, 1)

        self.user_bias = nn.Embedding(conf.num_users, 1)
        self.item_bias = nn.Embedding(conf.num_items, 1)

        self.reinit()

    def reinit(self):
        #for fc in [self.user_fc_linear, self.item_fc_linear]:
        #    nn.init.uniform_(fc.weight, -0.1, 0.1)
        #    nn.init.constant_(fc.bias, 0.1)

        self.gmf_user_embedding.weight = \
            torch.nn.Parameter(0.1 * self.gmf_user_embedding.weight)
        self.gmf_item_embedding.weight = \
            torch.nn.Parameter(0.1 * self.gmf_item_embedding.weight)

        #for idx in range(len(conf.mlp_dim_list)-1):
        #    nn.init.uniform_(self.linears[idx].weight, -0.1, 0.1)
        torch.manual_seed(0)
        nn.init.uniform_(self.linear_1.weight, -0.1, 0.1)
        nn.init.uniform_(self.linear_2.weight, -0.1, 0.1)
        nn.init.uniform_(self.linear_3.weight, -0.1, 0.1)

        #for idx in range(len(conf.mlp_dim_list)-1):
        #    nn.init.uniform_(self.x_linears[idx].weight, -0.1, 0.1)
        torch.manual_seed(0)
        nn.init.uniform_(self.x_linear_1.weight, -0.1, 0.1)
        nn.init.uniform_(self.x_linear_2.weight, -0.1, 0.1)
        nn.init.uniform_(self.x_linear_3.weight, -0.1, 0.1)

        nn.init.zeros_(self.user_bias.weight)
        nn.init.zeros_(self.item_bias.weight)

        nn.init.uniform_(self.x_final_linear.weight, -0.05, 0.05)
        nn.init.constant_(self.x_final_linear.bias, 0.0)

    def forward(self, user, item, aspect_user_embed, aspect_item_embed, epoch):
        gmf_user_embed = self.gmf_user_embedding(user)
        gmf_item_embed = self.gmf_item_embedding(item)
        
        gmf_concat_embed = torch.cat([gmf_user_embed, gmf_item_embed], dim=1)
        #for idx in range(len(conf.mlp_dim_list)-1):
        #    gmf_concat_embed = torch.relu(self.x_linears[idx](gmf_concat_embed))
        gmf_concat_embed = torch.relu(self.x_linear_1(gmf_concat_embed))
        gmf_concat_embed = torch.relu(self.x_linear_2(gmf_concat_embed))
        gmf_concat_embed = torch.relu(self.x_linear_3(gmf_concat_embed))

        mlp_user_embed = self.user_fc_linear(aspect_user_embed)
        mlp_item_embed = self.item_fc_linear(aspect_item_embed)

        mlp_concat_emebd = torch.cat([mlp_user_embed, mlp_item_embed], dim=1)
        #for idx in range(len(conf.mlp_dim_list)-1):
        #    mlp_concat_emebd = torch.relu(self.linears[idx](mlp_concat_emebd))
        mlp_concat_emebd = torch.relu(self.linear_1(mlp_concat_emebd))
        mlp_concat_emebd = torch.relu(self.linear_2(mlp_concat_emebd))
        mlp_concat_emebd = torch.relu(self.linear_3(mlp_concat_emebd))
        
        #final_embed = torch.cat([gmf_concat_embed, mlp_concat_emebd], dim=1)
        final_embed = 1.0*mlp_concat_emebd + 0.0*gmf_concat_embed

        prediction = self.x_final_linear(final_embed) + conf.avg_rating + \
            self.user_bias(user) + self.item_bias(item)

        #import pdb; pdb.set_trace()

        #if epoch == 3:
        #    import pdb; pdb.set_trace()

        return gmf_user_embed, gmf_item_embed, prediction.view(-1)


class uarm(nn.Module): 
    def __init__(self):
        super(uarm, self).__init__()

        self.word_embedding = nn.Embedding(conf.vocab_sz, conf.word_dim)
        self.rnn_out_linear = nn.Linear(conf.hidden_dim, conf.vocab_sz)
        
        self.encoder_1 = encoder_1()
        self.encoder_2 = encoder_2()
        self.decoder_1 = decoder_1()
        self.decoder_2 = decoder_fm()

        self.reinit()

    def reinit(self):
        nn.init.xavier_uniform_(self.rnn_out_linear.weight)
        nn.init.zeros_(self.rnn_out_linear.bias)
    
    # user, item, review_input, review_target
    def forward(self, user, item, label, review_input, review_target, review_aspect_mask, \
        user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent, epoch):

        aspect_user_embed, user_J_loss, user_U_loss = self.encoder_2(user_pos_sent, user_neg_sent, epoch)
        aspect_item_embed, item_J_loss, item_U_loss = self.encoder_2(item_pos_sent, item_neg_sent, epoch)

        #if epoch == 2:
        #    import pdb; pdb.set_trace()

        aspect_user_embed = torch.mean(aspect_user_embed.reshape(-1, conf.user_seq_num, conf.asp_dim), dim=1)
        aspect_item_embed = torch.mean(aspect_item_embed.reshape(-1, conf.item_seq_num, conf.asp_dim), dim=1)


        ####### RATING PREDICTION #######
        gmf_user_embed, gmf_item_embed, pred = self.decoder_2(user, item, aspect_user_embed, aspect_item_embed, epoch)
        rating_out_loss = F.mse_loss(pred, label, reduction='none')
        rating_obj_loss = F.mse_loss(pred, label, reduction='sum')

        ####### REVIEW GENERATION #######
        #user, item, review_input, review_target = values[0], values[1], values[3], values[4]
        gamma_u, gamma_i, beta_u, beta_i, hidden_state = self.encoder_1(user, item)

        #gamma_u = gmf_user_embed
        #gamma_i = gmf_item_embed

        #beta_u = aspect_user_embed
        #beta_i = aspect_item_embed

        outputs = []

        x_word_probit = []

        for t_input in review_input:
            input_vector = self.word_embedding(t_input.view(1, -1))
            #output, hidden_state = self.decoder(input_vector, hidden_state)

            slice_word_probit, hidden_state = self.decoder_1(input_vector, 
                hidden_state, 
                gamma_u,
                gamma_i,
                beta_u, 
                beta_i, 
                review_aspect_mask, aspect_user_embed, aspect_item_embed
            )

            x_word_probit.append(slice_word_probit)
        
        word_probit = torch.cat(x_word_probit, dim=0)

        #import pdb; pdb.set_trace()
        review_out_loss = F.cross_entropy(word_probit, review_target.reshape(-1), ignore_index=PAD, reduction='none')
        obj = F.cross_entropy(word_probit, review_target.reshape(-1), ignore_index=PAD)

        #if epoch == 3:
        #    import pdb; pdb.set_trace()

        obj = 1e-7 * obj + 1e-4*(user_J_loss+item_J_loss+user_U_loss+item_U_loss) + 1.0 * rating_obj_loss
        return review_out_loss, obj, rating_out_loss

    def predict_rating(self, user, item, label, user_pos_sent, user_neg_sent, \
        item_pos_sent, item_neg_sent):

        aspect_user_embed, user_J_loss, user_U_loss = \
            self.encoder_2(user_pos_sent, user_neg_sent, epoch=0)

        aspect_item_embed, item_J_loss, item_U_loss = \
            self.encoder_2(item_pos_sent, item_neg_sent, epoch=0)

        aspect_user_embed = torch.mean(aspect_user_embed.reshape(\
            -1, conf.user_seq_num, conf.asp_dim), dim=1)

        aspect_item_embed = torch.mean(aspect_item_embed.reshape(\
            -1, conf.item_seq_num, conf.asp_dim), dim=1)

        ####### RATING PREDICTION #######
        _, _, pred = self.decoder_2(user, item, aspect_user_embed, aspect_item_embed, epoch=0)

        rating_out_loss = F.mse_loss(pred, label, reduction='none')
        rating_obj_loss = F.mse_loss(pred, label, reduction='sum')

        return rating_out_loss
    
    def _sample_text_by_top_one(self, user, item, review_input, review_aspect_mask, \
        user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent):
        
        aspect_user_embed, user_J_loss, user_U_loss = self.encoder_2(user_pos_sent, user_neg_sent)
        aspect_item_embed, item_J_loss, item_U_loss = self.encoder_2(item_pos_sent, item_neg_sent)

        aspect_user_embed = torch.mean(aspect_user_embed.reshape(-1, conf.user_seq_num, conf.asp_dim), dim=1)
        aspect_item_embed = torch.mean(aspect_item_embed.reshape(-1, conf.item_seq_num, conf.asp_dim), dim=1)

        gmf_user_embed, gmf_item_embed, pred = self.decoder_2(user, item, aspect_user_embed, aspect_item_embed)

        gamma_u, gamma_i, beta_u, beta_i, hidden_state = self.encoder_1(user, item)

        #gamma_u = gmf_user_embed
        #gamma_i = gmf_item_embed

        #beta_u = aspect_user_embed
        #beta_i = aspect_item_embed
 
        next_word_idx = review_input[0]

        #import pdb; pdb.set_trace()
        sample_idx_list = [next_word_idx]
        for _ in range(conf.rev_len):
            input_vector = self.word_embedding(next_word_idx.view(1, -1))

            slice_word_probit, hidden_state = self.decoder_1(
                input_vector, 
                hidden_state, 
                gamma_u,
                gamma_i,
                beta_u, 
                beta_i, 
                review_aspect_mask, aspect_user_embed, aspect_item_embed
            )

            word_probit = slice_word_probit

            next_word_idx = torch.argmax(word_probit, 1)
            sample_idx_list.append(next_word_idx)
        
        #import pdb; pdb.set_trace()
        sample_idx_list = torch.stack(sample_idx_list, 1)#.reshape(-1, conf.rev_len)
        
        return sample_idx_list