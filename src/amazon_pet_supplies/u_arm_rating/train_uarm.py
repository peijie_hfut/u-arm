import os, sys, shutil
import torch
import torch.nn as nn
import torch.utils.data as data

import numpy as np

from time import time, strftime
from copy import deepcopy
from gensim.models import Word2Vec

import DataModule_uarm as data_utils
import config_uarm as conf

from evaluate import evaluate, evaluate_aspect

from Logging import Logging

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

def now():
    return str(strftime('%Y-%m-%d %H:%M:%S'))

def check_dir(file_path):
    import os
    save_path = os.path.dirname(os.path.abspath(file_path))
    if not os.path.exists(save_path):
        os.makedirs(save_path)

def tensorToScalar(tensor):
    return tensor.cpu().detach().numpy()

if __name__ == '__main__':
    ############################## CREATE MODEL ##############################
    from uarm import uarm
    model = uarm()

    model_params = model.state_dict()

    # Load the word2vec and K_means parameters
    word_embedding = Word2Vec.load('%s/%s.wv.model' % (conf.target_path, conf.data_name))
    for idx in range(1):
        model_params['encoder_2.word_embedding.weight'][idx] = torch.zeros(conf.abae_word_dim)
    for idx in range(1, conf.abae_vocab_sz):
        model_params['encoder_2.word_embedding.weight'][idx] = torch.FloatTensor(word_embedding.wv[word_embedding.wv.index2entity[idx-1]])

    k_means_weight = np.load('%s/%s.k_means_%d.npy' % (conf.target_path, conf.data_name, conf.asp_dim)) # (aspect_dimesion, word_dimension)
    model_params['encoder_2.transform_T.weight'] = torch.FloatTensor(k_means_weight.transpose()) # (word_dim,  asp_dim)
    
    model.encoder_2.word_embedding.weight.requires_grad = False

    # Load the pretrained NCF parameters
    pretrain_params = torch.load('%s/out/%s/train_%s_ncf_id_X1.mod' % (conf.root_path, conf.data_name, conf.data_name))

    model_params['decoder_2.gmf_user_embedding.weight'] = pretrain_params['mlp_user_embedding.weight']
    model_params['decoder_2.gmf_item_embedding.weight'] = pretrain_params['mlp_item_embedding.weight']
    model_params['decoder_2.x_linear_1.weight'] = pretrain_params['linear_1.weight']
    model_params['decoder_2.x_linear_2.weight'] = pretrain_params['linear_2.weight']
    model_params['decoder_2.x_linear_3.weight'] = pretrain_params['linear_3.weight']
    model_params['decoder_2.x_final_linear.weight'] = pretrain_params['final_linear.weight']
    model_params['decoder_2.x_final_linear.bias'] = pretrain_params['final_linear.bias']
    model_params['decoder_2.user_bias.weight'] = pretrain_params['user_bias.weight']
    model_params['decoder_2.item_bias.weight'] = pretrain_params['item_bias.weight']

    model.decoder_2.gmf_user_embedding.weight.requires_grad = False
    model.decoder_2.gmf_item_embedding.weight.requires_grad = False
    model.decoder_2.x_linear_1.weight.requires_grad = False
    model.decoder_2.x_linear_2.weight.requires_grad = False
    model.decoder_2.x_linear_3.weight.requires_grad = False
    #model.decoder_2.x_final_linear.weight.requires_grad = False
    #model.decoder_2.x_final_linear.bias.requires_grad = False
    #model.decoder_2.user_bias.weight.requires_grad = False
    #model.decoder_2.item_bias.weight.requires_grad = False

    # Load the pretrained ABAE parameters
    pretrain_params = torch.load('%s/train_%s_abae_id_%d.mod' % (conf.out_path, conf.data_name, conf.asp_dim))
    model_params['encoder_2.word_embedding.weight'] = pretrain_params['word_embedding.weight']
    model_params['encoder_2.transform_M.weight'] = pretrain_params['transform_M.weight']
    model_params['encoder_2.transform_W.weight'] = pretrain_params['transform_W.weight']
    model_params['encoder_2.transform_T.weight'] = pretrain_params['transform_T.weight']

    model.load_state_dict(model_params)

    # Define the optimizers 
    parameters = []
    parameters.extend(model.decoder_2.user_fc_linear.parameters())
    parameters.extend(model.decoder_2.item_fc_linear.parameters())
    parameters.extend(model.decoder_2.linear_1.parameters())
    parameters.extend(model.decoder_2.linear_2.parameters())
    parameters.extend(model.decoder_2.linear_3.parameters())
    parameters.extend(model.decoder_2.x_final_linear.parameters())
    parameters.extend(model.decoder_2.user_bias.parameters())
    parameters.extend(model.decoder_2.item_bias.parameters())
    rating_optimizer = \
        torch.optim.Adam(parameters, lr=conf.learning_rate, weight_decay=conf.weight_decay)

    parameters = []
    parameters.extend(model.encoder_2.transform_M.parameters())
    parameters.extend(model.encoder_2.transform_W.parameters())
    parameters.extend(model.encoder_2.transform_T.parameters())
    abae_optimizer = \
        torch.optim.Adam(parameters, lr=conf.learning_rate)

    parameters = []
    parameters.extend(model.encoder_1.parameters())
    parameters.extend(model.decoder_1.parameters())
    review_optimizer = \
        torch.optim.Adam(parameters, lr=conf.learning_rate)

    #model.load_state_dict(torch.load('%s/train_%s_uarm_id_XF6_K_%d.mod' % (conf.out_path, conf.data_name, conf.asp_dim)))

    model.cuda()

    ############################## PREPARE DATASET ##############################
    print('System start to load data...')
    t0 = time()
    train_data, val_data, test_data, user_seq_dict, item_seq_dict = data_utils.load_all()

    #aspect_vocab = data_utils.construct_aspect_voab()

    t1 = time()
    print('Data has been loaded successfully, cost:%.4fs' % (t1 - t0))

    ########################### FIRST TRAINING #####################################
    check_dir('%s/train_%s_uarm_id_x.log' % (conf.out_path, conf.data_name))
    log = Logging('%s/train_%s_uarm_id_XF6.py' % (conf.out_path, conf.data_name))
    train_model_path = '%s/train_%s_uarm_id_XF6' % (conf.out_path, conf.data_name)

    # prepare data for the training stage
    train_dataset = data_utils.TrainData(train_data, user_seq_dict, item_seq_dict)
    train_batch_sampler = data.BatchSampler(data.RandomSampler(\
        range(train_dataset.length)), batch_size=conf.batch_size, drop_last=False)

    val_dataset = data_utils.TestData(val_data, user_seq_dict, item_seq_dict)
    val_batch_sampler = data.BatchSampler(data.RandomSampler(\
        range(val_dataset.length)), batch_size=conf.batch_size, drop_last=False)

    test_dataset = data_utils.TestData(test_data, user_seq_dict, item_seq_dict)
    test_batch_sampler = data.BatchSampler(data.RandomSampler(\
        range(test_dataset.length)), batch_size=conf.batch_size, drop_last=False)

    #'''
    review_aspect_index, review_aspect_value = train_dataset.construct_aspect_voab(model)
    review_aspect_mask = torch.sparse.FloatTensor(review_aspect_index.t(), \
        review_aspect_value, torch.Size([conf.gen_vocab_sz, conf.aspect_dim]))
    #'''
    
    # prepare data for the evaluation
    review_val_dataset = data_utils.TestData(val_data, user_seq_dict, item_seq_dict)
    review_val_sampler = data.BatchSampler(data.SequentialSampler(\
        range(review_val_dataset.length)), batch_size=conf.batch_size, drop_last=False)

    review_test_dataset = data_utils.TestData(test_data, user_seq_dict, item_seq_dict)
    review_test_sampler = data.BatchSampler(data.SequentialSampler(\
        range(review_test_dataset.length)), batch_size=conf.batch_size, drop_last=False)

    # Start Training !!!
    max_bleu = 0.0
    for epoch in range(1, conf.train_epochs+1):
        t0 = time()
        model.train()

        #'''
        train_review_loss = []
        for batch_idx_list in train_batch_sampler:
            
            user_list, item_list, label, review_input_list, review_output_list, \
                user_pos_sent, user_neg_sent, item_pos_sent, \
                item_neg_sent =\
                train_dataset.get_batch(batch_idx_list)

            review_out_loss, obj, rating_out_loss = model(user_list, item_list, label, review_input_list, \
                review_output_list, review_aspect_mask, user_pos_sent, \
                user_neg_sent, item_pos_sent, item_neg_sent, epoch)

            train_review_loss.extend(tensorToScalar(rating_out_loss))
            model.zero_grad(); obj.backward(); rating_optimizer.step()
        t1 = time()

        log.record('Training Stage: Epoch:{}, compute loss cost:{:.4f}s'.format(epoch, (t1-t0)))
        log.record('Train loss:{:.4f}'.format(np.mean(train_review_loss)))
        #'''

        # evaluate the performance of the model with following code
        model.eval()

        #'''
        val_pred = []
        val_loss = []
        for batch_idx_list in val_batch_sampler:
            user, item, label, review_input_list, real_review_list, \
                user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent = \
                val_dataset.get_batch(batch_idx_list)

            rating_out_loss = \
                model.predict_rating(user, item, label, user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent)
            
            val_loss.extend(tensorToScalar(rating_out_loss))
        t2 = time()

        log.record('Val cost:%.4fs' % (t2-t1))
        log.record('Val loss:{:.4f}'.format(np.sqrt(np.mean(val_loss))))

        min_test_loss = 10.0
        test_pred = []
        test_loss = []
        for batch_idx_list in test_batch_sampler:
            user, item, label, review_input_list, real_review_list, \
                user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent = \
                test_dataset.get_batch(batch_idx_list)

            rating_out_loss = \
                model.predict_rating(user, item, label, user_pos_sent, user_neg_sent, item_pos_sent, item_neg_sent)
            
            test_loss.extend(tensorToScalar(rating_out_loss))
        t3 = time()

        test_loss = np.sqrt(np.mean(test_loss))
        if test_loss < min_test_loss:
            #torch.save(model.state_dict(), '%s_K_%d.mod' % (train_model_path, conf.asp_dim))
            min_test_loss = min(test_loss, min_test_loss)

        log.record('Test cost:%.4fs' % (t3-t2))
        log.record('Test loss:{:.4f}'.format(test_loss))
        #'''