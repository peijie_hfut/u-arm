'''
from collections import defaultdict

csv_file_1 = open('/Users/sunpeijie/Mac/Task/[TOIS]Aspect修改/v1_期刊修改/实验记录/stat_8229872.csv')
csv_file_2 = open('/Users/sunpeijie/Mac/Task/[TOIS]Aspect修改/v1_期刊修改/实验记录/stat_8229411.csv')


model_name = ['Att2Seq', 'ExpansionNet', 'MRG', 'CF_GCN', 'U-ARM']

selection_dict = defaultdict(list)

flag = 0
start = 0
count = 0

for line in csv_file_1:
    if '选项' not in line and '有效填写' not in line and line != '\n' and '第1页' not in line:
        if 'GT:' in line:
            start = 0
            continue
        tmp = line.strip('\n').split(',')
        start += 1
        if int(tmp[-1]) > 0:
            if start <= 4:
                selection_dict[start].append(int(tmp[-1]))
            elif start > 4:
                selection_dict[5].append(int(tmp[-1]))
            count += int(tmp[-1])
        #print(start)
        #print(line)

for line in csv_file_2:
    if '选项' not in line and '有效填写' not in line and line != '\n' and '第1页' not in line:
        if 'GT:' in line:
            start = 0
            continue
        tmp = line.strip('\n').split(',')
        start += 1
        if int(tmp[-1]) > 0:
            if start <= 4:
                selection_dict[start].append(int(tmp[-1]))
            elif start > 4:
                selection_dict[5].append(int(tmp[-1]))
            count += int(tmp[-1])
        #print(start)
        #print(line)

print(count)

for key in range(1, 6):
    print('%s: Click:%d, Percentage:%.2f' % (model_name[key-1], sum(selection_dict[key]), sum(selection_dict[key])/count))
#'''

###### Alternative ######

from collections import defaultdict

csv_file_1 = open('/Users/sunpeijie/Mac/Task/[TOIS]Aspect修改/v1_期刊修改/实验记录/stat_8229872.csv')
csv_file_2 = open('/Users/sunpeijie/Mac/Task/[TOIS]Aspect修改/v1_期刊修改/实验记录/stat_8229411.csv')


model_name = ['Att2Seq', 'ExpansionNet', 'MRG', 'CF_GCN', 'U-ARM']

selection_dict = defaultdict(list)

flag = 0
start = 0
count = 0

for line in csv_file_1:
    if '选项' not in line and '有效填写' not in line and line != '\n' and '第1页' not in line:
        if 'GT:' in line:
            start = 0
            continue
        tmp = line.strip('\n').split(',')
        start += 1
        if int(tmp[-1]) > 0:
            selection_dict[start].append(int(tmp[-1]))
            count += int(tmp[-1])
        #print(start)
        #print(line)

for line in csv_file_2:
    if '选项' not in line and '有效填写' not in line and line != '\n' and '第1页' not in line:
        if 'GT:' in line:
            start = 0
            continue
        tmp = line.strip('\n').split(',')
        start += 1
        if int(tmp[-1]) > 0:
            selection_dict[start].append(int(tmp[-1]))
            count += int(tmp[-1])
        #print(start)
        #print(line)

print(count)

for key in range(1, 11):
    print('Key:%d: Click:%d, Percentage:%.2f' % (key, sum(selection_dict[key]), sum(selection_dict[key])/count))