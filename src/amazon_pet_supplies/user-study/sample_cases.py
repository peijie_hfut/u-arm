import xlwt
import random

path = '/root/dev/task/aspect_rs/out/amazon_pet_supplies'

Att2Seq_file = open('%s/out_att2seq.py' % path)
MRG_file = open('%s/out_mrg.py' % path)
CF_GCN_file = open('%s/out_cf_gcn.py' % path)
ExpansionNet_file = open('%s/out_expansion_net.py' % path)
GT_file = open('%s/ground_truth.py' % path)
U_ARM_1_file = open('%s/out_abae_mtl_1.py' % path)
U_ARM_2_file = open('%s/out_abae_mtl_2.py' % path)
U_ARM_3_file = open('%s/out_abae_mtl_3.py' % path)
U_ARM_4_file = open('%s/out_abae_mtl_4.py' % path)
U_ARM_5_file = open('%s/out_abae_mtl_5.py' % path)
U_ARM_6_file = open('%s/out_abae_mtl_6.py' % path)


#设置表格样式
def set_style(name,height,bold=False):
	style = xlwt.XFStyle()
	font = xlwt.Font()
	font.name = name
	font.color_index = 4
	font.height = height
	style.font = font
	return style

#写Excel
def write_excel():
    f = xlwt.Workbook()
    sheet1 = f.add_sheet('User-Study',cell_overwrite_ok=True)

    random_set = set()
    for col_idx in range(0, 50):
        random_idx = random.randint(0, len(Att2Seq))
        while random_idx in random_set:
            random_idx = random.randint(0, len(Att2Seq))
        random_set.add(random_idx)

        sheet1.write(0, col_idx, '[单选题]GT:%s' % GT[random_idx], set_style('Calibri (Body)', 220, True))

        sheet1.write(1, col_idx, Att2Seq[random_idx])
        sheet1.write(2, col_idx, ExpansionNet[random_idx])
        sheet1.write(3, col_idx, MRG[random_idx])
        sheet1.write(4, col_idx, CF_GCN[random_idx])
        sheet1.write(5, col_idx, U_ARM_1[random_idx])
        sheet1.write(6, col_idx, U_ARM_2[random_idx])
        sheet1.write(7, col_idx, U_ARM_3[random_idx])
        sheet1.write(8, col_idx, U_ARM_4[random_idx])
        sheet1.write(9, col_idx, U_ARM_5[random_idx])
        sheet1.write(10, col_idx, U_ARM_6[random_idx])

    f.save('/root/dev/task/aspect_rs/out/user-study.xls')


def load_data():
    Att2Seq, MRG, CF_GCN, ExpansionNet, \
    GT, U_ARM_1, U_ARM_2, U_ARM_3, \
    U_ARM_4, U_ARM_5, U_ARM_6 = [], [], [], [], [], [], [], [], [], [], []

    for line in Att2Seq_file:
        tmp = line.split('@}')
        Att2Seq.append(tmp[0])

    for line in MRG_file:
        tmp = line.split('@}')
        MRG.append(tmp[0])

    for line in CF_GCN_file:
        tmp = line.split('@}')
        CF_GCN.append(tmp[0])
    
    for line in ExpansionNet_file:
        tmp = line.split('@}')
        ExpansionNet.append(tmp[0])

    bias = -33
    for line in GT_file:
        GT.append(line[:bias])

    for line in U_ARM_1_file:
        U_ARM_1.append(line[:bias])

    for line in U_ARM_2_file:
        U_ARM_2.append(line[:bias])

    for line in U_ARM_3_file:
        U_ARM_3.append(line[:bias])
    
    for line in U_ARM_4_file:
        U_ARM_4.append(line[:bias])

    for line in U_ARM_5_file:
        U_ARM_5.append(line[:bias])

    for line in U_ARM_6_file:
        U_ARM_6.append(line[:bias])

    return Att2Seq, MRG, GT, CF_GCN, \
        ExpansionNet, U_ARM_1, U_ARM_2, \
        U_ARM_3, U_ARM_4, U_ARM_5, U_ARM_6

if __name__ == '__main__':
    Att2Seq, MRG, GT, CF_GCN, \
        ExpansionNet, U_ARM_1, U_ARM_2, \
        U_ARM_3, U_ARM_4, U_ARM_5, U_ARM_6 = load_data()

    write_excel()