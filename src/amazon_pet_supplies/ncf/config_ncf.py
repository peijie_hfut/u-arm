import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

avg_rating = 4.2297
num_users = 19853
num_items = 8510

gmf_embed_dim = 32
mlp_embed_dim = 32

mlp_dim_list = [64, 128, 64, 32]

weight_decay = 0.02
learning_rate = 0.002
batch_size = 256
train_epochs = 20

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path