import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

word_dim = 512
hidden_dim = 512

att_dim = 64
aspect_dim = 15

dropout = 0.1
learning_rate = 0.002
batch_size = 256
train_epochs = 50

abae_vocab_sz = 16602
gen_vocab_sz = 27505

vocab_sz = 27505

num_users = 19853
num_items = 8510
seq_len = 11
seq_num = 8
sum_len = 8
rev_len = 31
mf_dim = 32

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path