import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

avg_rating = 4.2297
num_users = 19853
num_items = 8510

seq_len = 11
seq_num = 8
sum_len = 8
user_seq_num = 9
item_seq_num = 12

abae_vocab_sz = 16602
gen_vocab_sz = 27505
vocab_sz = 27505

rev_len = 31
abae_word_dim = 200
asp_dim = 15
seq_len = 11
num_neg_sent = 1

mf_dim = 32
gmf_embed_dim = 32
mlp_embed_dim = 32
mlp_dim_list = [64, 128, 64, 32]

word_dim = 512
hidden_dim = 512
att_dim = 64
aspect_dim = 15

dropout = 0.1
weight_decay = 0.020
learning_rate = 0.002
batch_size = 256
train_epochs = 50

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path