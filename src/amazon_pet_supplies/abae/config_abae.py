import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

vocab_sz = 16602
seq_len = 11
sent_num = 8

word_dim = 200
asp_dim = 15
num_neg_sent = 20

lr_lambda = 1
learning_rate = 0.001
batch_size = 50
train_epochs = 20

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path