import os
root_path = os.path.abspath(os.path.join(os.getcwd(), "../../.."))
data_name = 'amazon_pet_supplies'

avg_rating = 4.2297
num_users = 19853
num_items = 8510

num_words = 16602
vocab_sz = 27505
rev_len = 31
seq_len = 11
sent_num = 8
user_seq_num = 9
item_seq_num = 22

mf_dim = 32
encoder_word_dim = 200
word_dim = 512
hidden_dim = 512
dropout = 0.1
weight_decay = 0.02
learning_rate = 0.002
batch_size = 256
train_epochs = 30

target_path = '%s/data/%s' % (root_path, data_name)
out_path = '%s/out/%s' % (root_path, data_name)
model_path = '%s/out/model' % root_path