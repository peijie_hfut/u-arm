
# An Unsupervised Aspect-aware Recommendation Model withExplanation Text Generation

This repository is the official implementation of [An Unsupervised Aspect-aware Recommendation Model withExplanation Text Generation]. Besides, we also provide the implementation of other baseline models which are used in our paper.  

## Requirements

To install requirements:
    
    pip install -r requirements.txt

## Datasets

The dataset is not contained in this repository. You can download the datasets here:

- [Amazon Pet Supplies](https://dubox.com/s/1EiNuedVROa5j7QCpnS3onQ) Password: qf3s 

## Training Procedures

Preparation:
1. git clone this repository
2. download the datasets
3. place the datasets to the data directory

Comparative baseline models:
1. cd src/amazon_pet_supplies/model_name
2. python train_mdel.py

U-ARM:
- Pretraining the ABAE model:
  1. cd src/amazon_pet_supplies/abae
  2. python train_abae.py

- Pretraining the NCF model:
  1. cd src/amazon_pet_supplies/ncf
  2. python train_ncf.py

- Pretraining the U-ARM rating prediction module:
  1. cd src/amazon_pet_supplies/u_arm_rating
  2. python train_uarm.py

- Training the U-ARM explanation text generation module:
  1. cd src/amazon_pet_supplies/u_arm_review
  2. python train_uarm.py